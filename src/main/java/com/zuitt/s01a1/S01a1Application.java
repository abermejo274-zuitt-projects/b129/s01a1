package com.zuitt.s01a1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class S01a1Application {

	public static void main(String[] args) {
		SpringApplication.run(S01a1Application.class, args);
	}

	// http://localhost:8080/api/hi?name=<name>
	@GetMapping("api/hi")
	public String hi(@RequestParam(value = "name", defaultValue = "user") String name) {
		return String.format("Hi, %s!", name);
	}

	// http://localhost:8080/api/course?name=<name>&course=<course>
	@GetMapping("api/course")
	public String course(@RequestParam(value = "name") String name, @RequestParam(value = "course") String course) {
	return String.format("Hi, %s! You are enrolled in %s.", name, course);
	}

}
